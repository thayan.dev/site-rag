CREATE TABLE IF NOT EXISTS `server_info` (
  `name` varchar(256) DEFAULT NULL DEFAULT '',
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `mvp_flux_csd` (
  `char_id` int(11) unsigned NOT NULL default '0',
  `kill` int(11) NOT NULL DEFAULT '0',
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`char_id`),
  KEY `char_id` (`char_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `pvp_flux_csd` (
  `char_id` int(11) unsigned NOT NULL default '0',
  `type` smallint(1) unsigned NOT NULL default '0',
  `kill` int(11) NOT NULL DEFAULT '0',
  `die` int(11) NOT NULL DEFAULT '0',
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`char_id`, `type`),
  KEY `char_id` (`char_id`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `guild_flux_csd` (
  `guild_id` int(11) unsigned NOT NULL default '0',
  `break` int(11) NOT NULL DEFAULT '0',
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`guild_id`),
  KEY `char_id` (`guild_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `ggvg_flux_csd` (
  `guild_id` int(11) unsigned NOT NULL default '0',
  `kill` int(11) NOT NULL DEFAULT '0',
  `die` int(11) NOT NULL DEFAULT '0',
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`guild_id`),
  KEY `char_id` (`guild_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `pguild_flux_csd` (
  `char_id` int(11) unsigned NOT NULL default '0',
  `break` int(11) NOT NULL DEFAULT '0',
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`char_id`),
  KEY `char_id` (`char_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE `cp_cmsnews` ADD COLUMN `type` smallint(1) NOT NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `item_cash_db` ADD COLUMN `date` datetime;
ALTER TABLE `item_cash_db2` ADD COLUMN `date` datetime;
UPDATE `item_cash_db` SET `date`=NOW();
UPDATE `item_cash_db2` SET `date`=NOW();