<?php
return array(
	// O Menu deve acompanhar o usu�rio na p�gina?
	'NavbarFixed'	=> false,
	
	// Mostrar Footer com estat�sticas e troca de tema no final do rodap�?
	'ShowFooter'	=> false,
	
	// Configura��es do Servidor
	'Server' => array(
		// Servidor de Login
		'login'	=> array(
			// IP do Servidor
			'ip'	=> 'localhost',
			
			// Porta
			'port'	=> 8888
		),
		
		// Servidor de Personagens
		'char'	=> array(
			// IP do Servidor
			'ip'	=> 'localhost',
			
			// Porta
			'port'	=> 2293
		),
		
		// Servidor de Mapas
		'map'	=> array(
			// IP do Servidor
			'ip'	=> 'localhost',
			
			// Porta
			'port'	=> 4217
		),
		
		// Arquivo de Cache aonde ser� salvo informa��es do servidor.
		'cache'	=> $this->themePath('cache/Server.cache',true),
		
		// Tempo para expirar o arquivo de cache, em milissegundos.
		'expire' => 600
	),
	
	// Configura��es de Contas
	'Groups'	=> array(
		0	=> 'Normal',
		1	=> 'VIP',
		60	=> 'GameMaster',
		99	=> 'Administrador'
	),
	
	// Configura��es de Not�cias
	'News' => array(
		// Quantidade de Not�cias que ser� mostrado na p�gina inicial.
		'home_rows' 	=> 5,
		
		// Tipos de Not�cias
		'type' => array(
			0 => array(
				'label' => 'Novidades',
				'class' => 'new',
			),
			1 => array(
				'label' => 'Atualiza��es',
				'class' => 'event',
			),
			2 => array(
				'label' => 'Eventos',
				'class' => 'update',
			),
			3 => array(
				'label' => 'Manuten��o',
				'class' => 'maintenance',
			),
		)
	),
	
	// Configura��es de Ranking
	// Quantidade de Resultados nos Rankings da Home.
	'rank_home_rows' => 6,
	
	// Configura��es do CashShop
	// Quantidade de Resultas na vitrine do CashShop.
	'cashshop_home_rows' => 10,
	
	// Configura��es de Castelos
	'Castles' => array(
		// 1� Edi��o
		0 => array(
			'name' => 'Feudo de Luina',
			'list' => array(
				0 => 'Sirius',
				1 => 'Astrum',
				2 => 'Canopus',
				3 => 'Ringel',
				4 => 'Acrux',
			),
		),
		1 => array(
			'name' => 'Feudo de Britonia',
			'list' => array(
				5 => 'Arsulf',
				6 => 'Trapesac',
				7 => 'Ruaden',
				8 => 'Saffran',
				9 => 'Arima'
			)
		),
		2 => array(
			'name' => 'Feudo do Bosque Celestial',
			'list' => array(
				10 => 'Pal�cio do Sol',
				11 => 'Pal�cio do Lago Sagrado',
				12 => 'Pal�cio da Sombra',
				13 => 'Pal�cio Escarlate',
				14 => 'Pal�cio da Colina'
			)
		),
		3 => array(
			'name' => 'Feudo das Valqu�rias',
			'list' => array(
				15 => 'Kriemhild',
				16 => 'Hrist',
				17 => 'Brynhildr',
				18 => 'Skoegul',
				19 => 'Gondul'
			)
		),
		// N Guild
		4 => array(
			'name' => 'Novice Castles',
			'list' => array(
				20 => 'Terra',
				21 => '�r',
				22 => '�gua',
				23 => 'Fogo',
			)
		),
		// 2� Edi��o
		5 => array(
			'name' => 'Feudo de Nithafjoll',
			'list' => array(
				24 => 'Himinn',
				25 => 'Andlangr',
				26 => 'Vidblainn',
				27 => 'Hljod',
				28 => 'Skatyrnir'
			)
		),
		6 => array(
			'name' => 'Feudo de Valfreyja',
			'list' => array(
				29 => 'Mardol',
				30 => 'Syr',
				31 => 'Horn',
				32 => 'Gefn',
				33 => 'Vanadis'
			)
		),
		// 3� Edi��o
		7 => array(
			'name' => 'Feudo de Gloria',
			'list' => array(
				34 => 'Gaebolg',
				35 => 'Richard',
				36 => 'Wigner',
				37 => 'Heine',
				38 => 'Nerius'
			)
		),
		8 => array(
			'name' => 'Kafra Garten',
			'list' => array(
				39 => 'Sofia',
				40 => 'Denise',
				41 => 'Marianne',
				42 => 'Bianca',
				43 => 'Deborah'
			)
		)
	),
	
	// Configura��es de Midia Social
	'Social' => array(
		// F�rum
		'board' => 'http://forum.ilha-ro.myrag.com.br/',
		
		// Facebook
		'facebook' => 'https://www.facebook.com/2IlhaRO',
		
		// Instagram
		'instagram' => '#b',
		
		// Discord
		'discord' => 'https://discord.gg/enMuE8q',
		
		// Youtube
		'youtube' => '#d',
		
		// Twitch
		'twitch' => '#e'
	),
	
	// M�ses
	'Months' => array(
		'abr' => array(
				 1 => 'JAN',
				 2 => 'FEV',
				 3 => 'MAR',
				 4 => 'ABR',
				 5 => 'MAI',
				 6 => 'JUN',
				 7 => 'JUL',
				 8 => 'AGO',
				 9 => 'SET',
				10 => 'OUT',
				11 => 'NOV',
				12 => 'DEZ'
		),
		'full' => array(
				 1 => 'Janeiro',
				 2 => 'Fevereiro',
				 3 => 'Mar�o',
				 4 => 'Abril',
				 5 => 'Maio',
				 6 => 'Junho',
				 7 => 'Julho',
				 8 => 'Agosto',
				 9 => 'Setembro',
				10 => 'Outubro',
				11 => 'Novembro',
				12 => 'Dezembro'
		)
	),
	
	// Classes que utilizam o mesmo sprite.
	'JobAliases' => array(
		13 => 7,
		21 => 14,
		4001 => 0,
		4002 => 1,
		4003 => 2,
		4004 => 3,
		4005 => 4,
		4006 => 5,
		4007 => 6,
		4014 => 4008,
		4022 => 4015,
		4023 => 0,
		4024 => 1,
		4025 => 2,
		4026 => 3,
		4027 => 4,
		4028 => 5,
		4029 => 6,
		4030 => 7,
		4031 => 8,
		4032 => 9,
		4033 => 10,
		4034 => 11,
		4035 => 12,
		4036 => 7,
		4037 => 14,
		4038 => 15,
		4039 => 16,
		4040 => 17,
		4041 => 18,
		4042 => 19,
		4043 => 19,
		4044 => 14,
		4045 => 25,
		4048 => 4047,
		4060 => 4054,
		4061 => 4055,
		4073 => 4066,
		4074 => 4067,
		4080 => 4054,
		4081 => 4054,
		4082 => 4066,
		4083 => 4066,
		4096 => 4054,
		4097 => 4055,
		4102 => 4066,
		4103 => 4067,
		4109 => 4054,
		4110 => 4066,
		4190 => 25,
		4191 => 25,
		4220 => 4218,
		4222 => 25,
		4223 => 4211,
		4224 => 4212,
		4225 => 4046,
		4226 => 4047,
		4227 => 4049,
		4228 => 24,
		4229 => 4215,
		4238 => 4047,
		4242 => 4240,
		4241 => 4239,
		4243 => 4239,
		4244 => 4239,
	),
	
	// Classes que o masculino � diferente do feminino.
	'JobAliasesGender' => array(
		// Masculinas
		'm' => array(
			20 => 19,
			4021 => 4020,
			4212 => 4211,
		),
		'f' => array(
			19 => 20,
			4020 => 4021,
			4411 => 4212,
		)
	),
	
	// Vitrine de Itens
	'ItemShop' => array(
		'url' => '#',
		'list' => array(
			18740 => array(
				'name' => 'Super Saiyajin',
				'price' => '1.000',
				'type' => 'Cura',
				'desc' => array(
					'Po��o feita de ervas vermelhas.',
					'Recupera cerca de 45 HP, dependendo da sua vitalidade.',
					'D� um clique duplo neste item para recuperar uma quantia de HP.',
					'Peso: 7'
				)
			),
			19996 => array(
				'name' => 'Cavalo Rei',
				'price' => '1.000',
				'type' => 'Cura',
				'desc' => array(
					'Po��o feita de ervas vermelhas e amarelas.',
					'Recupera cerca de 105 HP, dependendo da sua vitalidade.',
					'Peso: 20'
				)
			),
			18540 => array(
				'name' => 'M�scara do Evangelho',
				'price' => '1.000',
				'type' => 'Cura',
				'desc' => array(
					'Po��o feita de ervas amarelas.',
					'Recupera cerca de 175 HP, dependendo da sua vitalidade.',
					'Peso: 13'
				)
			),
			19746 => array(
				'name' => 'Cora��o Gelado',
				'price' => '1.000',
				'type' => 'Cura',
				'desc' => array(
					'Po��o feita de ervas amarelas.',
					'Recupera cerca de 175 HP, dependendo da sua vitalidade.',
					'Peso: 13'
				)
			),
			5421 => array(
				'name' => 'Orelhas do Ifrit',
				'price' => '1.000',
				'type' => 'Cura',
				'desc' => array(
					'Po��o feita de ervas brancas.',
					'Recupera cerca de 325 HP, dependendo da sua vitalidade.',
					'Peso: 15'
				)
			)
		),
	)
);
?>
