﻿<?php if (!defined('FLUX_ROOT')) exit; ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Ragnarok-Online">
	<meta name="author" content="Creative Services and Development">

	<?php if (isset($metaRefresh)): ?>
	<meta http-equiv="refresh" content="<?php echo $metaRefresh['seconds'] ?>; URL=<?php echo $metaRefresh['location'] ?>" />
	<?php endif ?>
	
	<title>::: <?php echo Flux::config('SiteTitle'); if (isset($title)) echo " - $title" ?> :::</title>
	<link rel="shortcut icon" href="<?php echo $this->themePath('images/favicon.png') ?>"/>
	<link rel="canonical" href="<?php echo $this->url('main') ?>">
	
	<link rel="stylesheet" href="<?php echo $this->themePath('css/flux.css') ?>" type="text/css" media="screen" title="" charset="utf-8" />
	<link href="<?php echo $this->themePath('css/flux/unitip.css') ?>" rel="stylesheet" type="text/css" media="screen" title="" charset="utf-8" />

	<?php if (Flux::config('EnableReCaptcha')): ?>
		<script src='https://www.google.com/recaptcha/api.js'></script>
	<?php endif ?>
	
	<!--[if IE]>
	<link rel="stylesheet" href="<?php echo $this->themePath('css/flux/ie.css') ?>" type="text/css" media="screen" title="" charset="utf-8" />
	<![endif]-->
	
	<!-- jQuery -->
	<script src="<?php echo $this->themePath('js/modernizr.js') ?>"></script>
	<script src="<?php echo $this->themePath('js/jquery-3.2.1.min.js') ?>"></script>
	
	<!-- Bootstrap 4 -->
    <link href="<?php echo $this->themePath('css/bootstrap.min.css') ?>" rel="stylesheet">
    <script src="<?php echo $this->themePath('js/popper.min.js') ?>"></script>
    <script src="<?php echo $this->themePath('js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo $this->themePath('js/bootstrap.bundle.min.js') ?>"></script>
	
	
	<!-- Font Awesome -->
	<link href="<?php echo $this->themePath('css/fontawesome.min.css?v=1') ?>" rel="stylesheet">
	
	<!-- Animate Slider -->
	<link href="<?php echo $this->themePath('css/jquery.animateSlider.css') ?>" rel="stylesheet">
	<script src="<?php echo $this->themePath('js/jquery.animateSlider.min.js') ?>"></script>
	<script src="<?php echo $this->themePath('js/slider.js') ?>"></script>
	
	<!-- Owl Carousel -->
	<link href="<?php echo $this->themePath('css/owl.carousel.min.css') ?>" rel="stylesheet">
	<script src="<?php echo $this->themePath('js/owl.carousel.min.js') ?>"></script>
	
	<!-- FancyBox -->
	<link rel="stylesheet" href="<?php echo $this->themePath('css/jquery.fancybox.min.css') ?>">
	<script src="<?php echo $this->themePath('js/jquery.fancybox.min.js') ?>"></script>
	
	<!-- Template -->
    <link href="<?php echo $this->themePath('css/animate.css') ?>" rel="stylesheet">
    <link href="<?php echo $this->themePath('css/creativesd.min.css') ?>?v=33" rel="stylesheet">
    <link href="<?php echo $this->themePath('css/responsive.min.css') ?>?v=77" rel="stylesheet">
	<script src="<?php echo $this->themePath('js/flux.datefields.js') ?>"></script>
	<script src="<?php echo $this->themePath('js/jquery.waypoints.min.js') ?>"></script>
	<script src="<?php echo $this->themePath('js/creativesd.min.js') ?>?v=14"></script>
	<script src="<?php echo $this->themePath('js/modalDialogs.min.js') ?>"></script>
    <script src="<?php echo $this->themePath('js/bootstrap-notify.min.js') ?>"></script>
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans|Open+Sans+Condensed:300&display=swap" rel="stylesheet">
  </head>

  <body>
  
	<?php
		// Read CreativeSD Class
		require_once $this->themePath('lib/CreativeSD.class.php', true);
	
		// Define Current Module
		define('CURRENT_MODULE', $params->get('module'));
		
		// Define Is Index
		define('MAIN_INDEX', $params->get('module') == 'main' ? true : false);
		
		// Instance CreativeSD Class
		$Creative = new Creativesd(require('config.php'), $this, $server);
		
		// Define URL
		define('CURRENT_URL', $Creative->getURL());
		
		include $this->themePath('main/navbar.php', true);
		include $this->themePath('main/submenu.php', true);
	?>
	
	<div class="container<?php echo MAIN_INDEX === true ? '-fluid main-content' : ' sub-content' ?>">
	
		<?php if (Flux::config('DebugMode') && @gethostbyname(Flux::config('ServerAddress')) == '127.0.0.1'): ?>
			<p class="notice">Please change your <strong>ServerAddress</strong> directive in your application config to your server's real address (e.g., myserver.com).</p>
		<?php endif ?>
								
		<?php if ($message=$session->getMessage()): ?>
			<!-- Messages -->
			<p class="message"><?php echo htmlspecialchars($message) ?></p>
		<?php endif ?>
								
			<!-- Page menu -->
		<?php include $this->themePath('main/pagemenu.php', true) ?>