<?php
if (!defined('FLUX_ROOT')) exit;

$CCfg = require($this->themePath('config.php', true));

$title    = 'Top Jogadores GvG';
$classes  = Flux::config('JobClasses')->toArray();
$jobClass = $params->get('jobclass');
$bind     = array();
$sqlpartial = '';

if (trim($jobClass) === '') {
	$jobClass = null;
}

if (!is_null($jobClass) && !array_key_exists($jobClass, $classes)) {
	$this->deny();
}

$col = "`rank`.`char_id` AS `char_id`, `rank`.`kill` AS `kill`, `rank`.`die` AS `die`, (`rank`.`kill`-`rank`.`die`) AS ration,  `rank`.`last_update` AS `last_update`, ";
$col .= "`ch`.`char_id`, `ch`.`name` AS `name`, `ch`.`class` AS `job`, `ch`.`base_level` AS `base_level`, `ch`.`job_level` AS `job_level`, `ch`.`sex` AS `sex`, ";
$col .= "`ch`.`guild_id`, `guild`.`name` AS guild_name, `guild`.`emblem_len` AS `guild_emblem_len`";

$sqlpartial .= "FROM {$server->charMapDatabase}.`pvp_flux_csd` AS `rank`";
$sqlpartial .= "LEFT JOIN `{$server->charMapDatabase}`.`char` AS `ch` ON `ch`.`char_id`=`rank`.`char_id` ";
$sqlpartial .= "LEFT JOIN {$server->charMapDatabase}.`guild` AS `guild` ON `guild`.`guild_id` = `ch`.`guild_id` ";
$sqlpartial .= "LEFT JOIN {$server->loginDatabase}.`login` AS `login` ON `login`.`account_id` = `ch`.`account_id` ";

$sqlpartial .= "WHERE `rank`.`type`='1' ";

if (Flux::config('HidePermBannedCharRank')) {
	$sqlpartial .= "AND `login`.`state`!='5' ";
}
if (Flux::config('HideTempBannedCharRank')) {
	$sqlpartial .= "AND (`login`.`unban_time` IS NULL OR `login`.`unban_time`='0') ";
}

$groups = AccountLevel::getGroupID((int)Flux::config('RankingHideGroupLevel'), '<');
if(!empty($groups)) {
	$ids   = implode(', ', array_fill(0, count($groups), '?'));
	$sqlpartial  .= "AND `login`.`group_id` IN ($ids) ";
	$bind  = array_merge($bind, $groups);
}

if ($days=Flux::config('CharRankingThreshold')) {
	$sqlpartial .= 'AND TIMESTAMPDIFF(DAY, `login`.`lastlogin`, NOW()) <= ? ';
	$bind[] = $days * 24 * 60 * 60;
}

$jobList = array();
if( $jobClass != null ) {
	$jobList[] = $jobClass;
	foreach( $CCfg['JobAliases'] as $job_id1 => $job_id2 ) {
		if( $job_id1 == $jobClass && !in_array($job_id2,$jobList) ) {
			$jobList[] = $job_id2;
		}
		if( $job_id2 == $jobClass && !in_array($job_id1,$jobList) ) {
			$jobList[] = $job_id1;
		}
	}
	$ids  = implode(', ', array_fill(0, count($jobList), '?'));
	$sqlpartial .= "AND `ch`.`class` IN ($ids) ";
	$bind  = array_merge($bind, $jobList);
}

//$sql = "SELECT $col {$sqlpartial} ORDER BY ration DESC, last_update DESC LIMIT 1";//.(int)Flux::config('CharRankingLimit');
//$sth  = $server->connection->getStatement($sql);
//$sth->execute($bind);
//$chars = $sth->fetchAll();

// Get total count and feed back to the paginator.
$sth = $server->connection->getStatement("SELECT COUNT(DISTINCT `rank`.`char_id`) AS total $sqlpartial");
$sth->execute($bind);

$total = $sth->fetch()->total;
$paginator = $this->getPaginator($total);
$sortable = array('ration' => 'desc', 'last_update' => 'asc');
$paginator->setSortableColumns($sortable);

$sql  = $paginator->getSQL("SELECT $col $sqlpartial GROUP BY `rank`.`char_id`");
$sth  = $server->connection->getStatement($sql);

$sth->execute($bind);
$chars = $sth->fetchAll();
?>
