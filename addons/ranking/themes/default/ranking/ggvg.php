<?php if (!defined('FLUX_ROOT')) exit; 
	$limit=(int)Flux::config('CharRankingLimit');
	if( $limit > $total )
		$limit = $total;
?>
<h2 class="head">Top Clãs GvG</h2>
<p class="sub-head">
	Listando <?php echo number_format($limit) ?> de <?php echo number_format($total) ?>  Clãs
</p>
<?php if ($guilds): ?>
<table class="table table-green">
	<thead>
		<tr>
			<th class="text-center" data-toggle="tooltip-xs" title="Posição">Pos<span class="d-none d-sm-none d-md-inline">ição</span></th>
			<th>Clã</th>
			<th>Mestre</th>
			<th class="text-center" data-toggle="tooltip-xs" title="Total de Membros">T<span class="d-none d-sm-none d-md-inline">otal de </span>M<span class="d-none d-sm-none d-md-inline">embros</span></th>
			<th class="text-center" data-toggle="tooltip-xs" title="Castelos Conquistados">C<span class="d-none d-sm-none d-md-inline">astelos </span>C<span class="d-none d-sm-none d-md-inline">onquistados</span></th>
			<th class="text-center" data-toggle="tooltip-xs" title="Eliminações">E<span class="d-none d-sm-none d-md-inline">liminações</span></th>
			<th class="text-center" data-toggle="tooltip-xs" title="Mortes">M<span class="d-none d-sm-none d-md-inline">ortes</span></th>
			<th class="text-center" data-toggle="tooltip-xs" title="Proporção"><?php echo htmlspecialchars_decode($paginator->sortableColumn('ration', 'P<span class="d-none d-sm-none d-md-inline">roporção</span>')) ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
			$i=0;
			foreach( $guilds as $grow ):
				++$i;
				$guildname = "";
				$sex = $grow->sex == 'F' ? 'f' : 'm';
				$class = isset($Creative->Cfg['JobAliasesGender'][$sex][$grow->job]) ? $Creative->Cfg['JobAliasesGender'][$sex][$grow->job] : $grow->job;
				$icon = isset($Creative->Cfg['JobAliases'][$class]) ? $Creative->Cfg['JobAliases'][$class] : $class;
				$charname = '<span data-toggle="tooltip-xs" title="<strong>Nível de Base:</strong> ' . $grow->base_level . '<br/><strong>Nível de Classe:</strong> ' . $grow->job_level . '">' . $grow->name . '</span>';
				
				// Members
				$members = $Creative->getGuildMembers($grow->guild_id);
				$membersList = array();
				foreach( $members as $gmrow )
					$membersList[] = $gmrow->name;
				$membersList = '<strong>Membros do Clã:</strong><br/> ' . implode('<br/>', $membersList);
				
				// Castles
				$castles = $Creative->getGuildCastle($grow->guild_id);
				$castlesList = array();
				foreach($castles as $cd)
					$castlesList[] = $Creative->getCastleName($cd->castle_id);
								
				if( count($castlesList) )
					$castlesList = '<strong>Castelos sobre o poder do Clã:</strong><br/> ' . implode('<br/>', $castlesList);
				else
					$castlesList = 'Nenhum Castelo conquistado pelo Clã.';
		?>
		<tr>
			<td class="text-center"><?php echo number_format($i) ?></td>
			<td>
			<?php if ($grow->guild_emblem_len):
				$guildname = '<img src="' . $this->emblem($grow->guild_id) . '" /> ';
			?>
			<?php endif; ?>
			<?php
				  $guildname .= $grow->guild_name;
			?> 
			<?php if (!$grow->guild_emblem_len) ?>
				<?php if ($auth->actionAllowed('guild', 'view') && $auth->allowedToViewGuild): ?>
					<?php echo htmlspecialchars_decode($this->linkToGuild($grow->guild_id, $guildname)) ?>
				<?php else: ?>
					<?php echo $guildname ?>
				<?php endif ?>
			</td>
			<?php
				$char_name = '<img src="' . $this->themePath('images/class/guild/' . $icon . '.png') . '"> '.$grow->name;
			?>
			<td><strong data-toggle="tooltip-xs" data-html="true" title="<strong>Nível de Base:</strong> <?php echo $grow->base_level ?><br/><strong>Nível de Classe:</strong> <?php echo $grow->job_level ?>">
				<?php if ($auth->actionAllowed('character', 'view') && $auth->allowedToViewCharacter): ?>
					<?php echo htmlspecialchars_decode($this->linkToCharacter($grow->char_id, $char_name)) ?>
				<?php else: ?>
					<?php echo $char_name ?>
				<?php endif ?>
			</strong></td>
			<td class="text-center"><span  data-toggle="tooltip" data-html="true" title="<?php echo $membersList ?>"><?php echo number_format(count($members)) ?></span></td>
			<td class="text-center"><span  data-toggle="tooltip" data-html="true" title="<?php echo $castlesList ?>"><?php echo number_format(count($castles)) ?></span></td>
			<td class="text-center"><?php echo number_format($grow->kill) ?></td>
			<td class="text-center"><?php echo number_format($grow->die) ?></td>
			<td class="text-center"><?php echo number_format($grow->ration) ?></td>
		</tr>
	<?php endforeach ?>
	</tbody>
</table>
<?php echo $paginator->getHTML() ?>
<?php else: ?>
<p>Não há Clãs. <a href="javascript:history.go(-1)">Voltar</a>.</p>
<?php endif ?>
