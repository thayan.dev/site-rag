<?php
if (!defined('FLUX_ROOT')) exit;
//$newslimit = (int)Flux::config('CMSNewsLimit');
$title = 'Notícias';

$news = Flux::config('FluxTables.CMSNewsTable'); 
$dataInit = $params->get('date-init');
$dataEnd = $params->get('date-end');
$desc = $params->get('desc');
$category = $params->get('category');
$bind = array();

$sqlpartial = "WHERE 1=1 ";
if (trim($desc) !== '') {
	$sqlpartial .= "AND (title LIKE ? OR body LIKE ?) ";
	$bind[] = "%$desc%";
	$bind[] = "%$desc%";
}

if (trim($category) !== '') {
	$sqlpartial .= "AND type=? ";
	$bind[] = $category;
}

if (trim($dataInit) !== '' && trim($dataEnd) === '') {
	$startDate = $dataInit . " 00:00:00";
	$sqlpartial .= "AND created >= ? ";
	$bind[] = $startDate;
}
else if (trim($dataInit) !== '' && trim($dataEnd) !== '') {
	$startDate = $dataInit . " 00:00:00";
	$endDate = $dataEnd . " 23:59:59";
	$sqlpartial .= "AND (created >= ? AND created <= ?) ";
	$bind[] = $startDate;
	$bind[] = $endDate;
}
else if (trim($dataInit) === '' && trim($dataEnd) !== '') {
	$endDate = $dataEnd . " 23:59:59";
	$sqlpartial .= "AND created <= ? ";
	$bind[] = $endDate;
}

$sql = "FROM {$server->loginDatabase}.$news {$sqlpartial}";
$sth = $server->connection->getStatement("SELECT COUNT(DISTINCT id) AS total {$sql}");
$sth->execute($bind);
$total = $sth->fetch()->total;
$paginator = $this->getPaginator($total);
$sortable = array('id' => 'desc', 'created' => 'asc');
$paginator->setSortableColumns($sortable);

$sql  = $paginator->getSQL("SELECT id, type, title, body, link, author, created, modified {$sql}");
$sth  = $server->connection->getStatement($sql);
$sth->execute($bind);
$news = $sth->fetchAll();
?>
